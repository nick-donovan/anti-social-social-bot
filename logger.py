# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.

import logging


def setup_logger(log_file='bot.log'):
    logging.basicConfig(
        format="%(asctime)s - %(name)s - %(levelname)s - %(message)s",
        level=logging.INFO,
        filename=log_file,
    )

    logging.getLogger("httpx").setLevel(logging.WARNING)

    return logging.getLogger(__name__)
