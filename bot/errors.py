# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.

class AssError(Exception):
    """Base class for all ASS Bot exceptions."""

    def __init__(self, message='', error_labels=None):
        super(AssError, self).__init__(message)
        self._message = message
        self._error_labels = set(error_labels or [])


class URLError(AssError):
    """Raised when there's an error with the given URL."""


class MissingURL(URLError):
    """Raised when the bot couldn't find a URL within a message."""


class DangerousURL(URLError):
    """Raised when the provided URL could be dangerous."""


class UnsupportedURL(URLError):
    """Raised when the URL domain is unsupported."""
