# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.
import logging

from telegram import Update
from telegram.ext import ContextTypes

logger = logging.getLogger(__name__)


async def help_handler(update: Update, context: ContextTypes.DEFAULT_TYPE):
    help_text = """The Anti-Social Social Bot is intended to help people avoid social media while still maintaining the ability to share those special moments. By default the Anti-Social Social Bot will listen to where it can help in group or you can use the /ass command to call the bot directly."""

    await context.bot.send_message(
        chat_id=update.effective_chat.id,
        text=help_text
    )
