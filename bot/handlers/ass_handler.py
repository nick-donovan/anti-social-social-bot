# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.

import logging
import random
import re
import subprocess
import time
import yt_dlp

from datetime import datetime
from pathlib import Path
from telegram import Update
from telegram.constants import ChatType, ReactionEmoji
from telegram.error import NetworkError as TGNetworkError
from telegram.ext import ContextTypes
from urllib.parse import urlparse, quote, urlunparse
from yt_dlp.utils import DownloadError as YTDDownloadError

from bot.errors import MissingURL, UnsupportedURL, DangerousURL
from constants import Constants

logger = logging.getLogger(__name__)
ydl_logger = logging.getLogger('yt_dlp')


async def passive_call(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """ASS Bot has detected a link it can possibly use. This will not return errors to the callee if they arise."""

    # All private calls are treated as direct calls.
    if update.effective_chat.type == ChatType.PRIVATE:
        await ass_handler(update, context)
    else:
        await ass_handler(update, context, notify=False)


async def direct_call(update: Update, context: ContextTypes.DEFAULT_TYPE):
    """ASS Bot as been directly called with the /ass command. This will return errors to the callee if they arise."""
    await ass_handler(update, context)


async def ass_handler(update: Update, context: ContextTypes, notify=True):
    """Main driver for the ASS Bot."""
    text = update.effective_message.text
    path = ""
    try:
        # Parse
        url = get_url(text)
        url_config = get_domain_config(url)

        # Retrieve
        filename = f"{update.effective_user.id}_{datetime.utcnow().timestamp()}"
        path = dv(url, url_config, filename)

        # Notify
        if notify:
            await update.effective_message.set_reaction(ReactionEmoji.THUMBS_UP)

        # Send
        await update.effective_message.reply_video(
            video=open(path, 'rb'),
            caption=url,
            supports_streaming=True,
            disable_notification=(not notify)
        )

    except (MissingURL, UnsupportedURL, DangerousURL, YTDDownloadError, TGNetworkError) as e:
        if notify:
            error_message = get_error_msg(e)
            await update.effective_message.reply_text(error_message)

        # Delete
        if Path(path).is_file():
            Path.unlink(path)

        raise e


def get_url(text):
    """Return a sanitized URL for further processing."""
    url = parse_url(text)
    if url is None:
        raise MissingURL("Couldn't find a URL for this message.")

    url = sanitize_url(url)
    return url


def parse_url(s: str):
    """Look for a URL in the string."""
    pattern = r"(https?://[^\s]+)"
    search = re.search(pattern, s, re.IGNORECASE)
    if search:
        return search.group(1)
    else:
        return None


def sanitize_url(url):
    """Return the sanitized URL."""
    parsed_url = urlparse(url)

    # Basic inclusion check
    inclusion = parse_url(parsed_url.query)
    if inclusion:
        raise DangerousURL("Double check the URL!")  # todo add /report option

    scheme = parsed_url.scheme.lower() if parsed_url.scheme else "http"
    netloc = parsed_url.netloc.lower()
    path = quote(parsed_url.path)
    params = quote(parsed_url.params, safe='=&')
    query = quote(parsed_url.query, safe='=&')
    fragment = quote(parsed_url.fragment, safe='=&')

    url = urlunparse((
        scheme,
        netloc,
        path,
        params,
        query,
        fragment
    ))

    return url


def get_domain_config(url):
    """Return the domain-specific configuration from the URL or raise an exception."""
    config = retrieve_config(url)
    if not config:
        raise UnsupportedURL("This URL isn't supported yet.")  # todo add /request option

    return config


def retrieve_config(url):
    """Return the domain config if the url is supported."""
    parsed_url = urlparse(url)

    url_domain = parsed_url.netloc.lower()
    for domain in Constants.SUPPORTED_URLS:  # SUPPORTED_SITES: dict
        if domain.get("domain") in url_domain:
            return domain
    return None


def dv(url, url_config, filename):
    """Retrieve the requested video and return the path."""
    format_str = "bv*[ext=mp4][height<=720]+ba[ext=m4a]"  # best video that's an MP4 and 720p and best audio that's m4a
    format_str = f"{format_str}/b[ext=mp4][height<=720]"  # Or best video that's an MP4 and 720p or less and best audio
    format_str = f"{format_str}/bv*[height<=720]+ba"  # Or best video that's 720p or less and best audio
    format_str = f"{format_str}/bv*+ba"  # Or the best video and the best audio
    format_str = f"{format_str}/b"  # Or the best video/audio

    ydl_opts = {
        'format': format_str,
        'logger': ydl_logger,
        'sort': 'vcodec',
        'verbose': True,
        'outtmpl': f"{Constants.DIRECTORY}/{filename}.%(ext)s",
    }

    # Get the cookie file (if applicable)
    cf = url_config.get("cookie")
    if cf:
        ydl_opts['cookiefile'] = cf

    try:
        # Perform the request
        path = save_and_get_path(url, ydl_opts)
        return path
    except YTDDownloadError as e:
        # If wireguard is enabled try again
        if Constants.WIREGUARD:
            # Try to connect to wireguard max_tries times
            # If a configuration file fails, a new one is selected
            max_tries = 3
            confs = list(range(1, Constants.WIREGUARD_CONFIGS + 1))
            for _ in range(max_tries):
                conf = random.choice(confs)
                try:
                    if activate_wireguard(conf) != 0 and reset_wireguard(conf) != 0:
                        logger.error(f"Wireguard activation failed: {conf}")
                        confs.remove(conf)
                        continue
                    path = save_and_get_path(url, ydl_opts)
                    return path
                except YTDDownloadError:
                    pass
                finally:
                    deactivate_wireguard(conf)
        else:
            raise e


def activate_wireguard(conf):
    logger.info(f"Activating wireguard: {conf}")
    result = execute_command(["wg-up", str(conf)])
    logger.info(f"Wireguard activation: {result.stderr.decode()}")

    # Wait for wireguard to fully activate before proceeding
    time.sleep(10)
    return result.returncode


def deactivate_wireguard(conf):
    logger.info(f"Deactivating wireguard: {conf}")
    result = execute_command(["wg-down", str(conf)])
    logger.info(f"Wireguard deactivation: {result.stderr.decode()}")

    # Wait for wireguard to fully deactivate before proceeding
    time.sleep(10)
    return result.returncode


def reset_wireguard(conf):
    logger.info(f"Resetting wireguard: {conf}")
    deactivate_wireguard(conf)
    return activate_wireguard(conf)


def execute_command(command: list):
    result = subprocess.run(command, stdout=subprocess.PIPE, stderr=subprocess.PIPE)
    return result


def save_and_get_path(url, ydl_opts):
    with yt_dlp.YoutubeDL(ydl_opts) as ydl:
        info = ydl.extract_info(url)
        info = ydl.sanitize_info(info)
    # Return the path
    request = info["requested_downloads"][0]
    path = request["filepath"]
    return path


def get_error_msg(e):
    error_message = str(e)
    if isinstance(e, YTDDownloadError):
        error_message = "Something went wrong with the download. Try again?"
    elif isinstance(e, TGNetworkError) and "large" in str(e).lower():  # todo find better way to check this
        error_message = "File too large!"
    return error_message
