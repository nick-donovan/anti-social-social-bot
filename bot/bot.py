# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.

import logging

from telegram import Update
from telegram.ext import ApplicationBuilder, CommandHandler, filters, MessageHandler

from bot.handlers import start_handler, help_handler, error_handler, direct_call, passive_call
from constants import Constants

logger = logging.getLogger(__name__)


class Bot:
    def __init__(self, token):
        self.token = token

    def start(self):
        """Start the bot and begin listening for commands and messages."""
        application = (ApplicationBuilder()
                       .token(self.token)
                       .base_url(f"{Constants.SERVER_URL}/bot")
                       .local_mode(True)
                       .base_file_url(f"{Constants.SERVER_URL}/file/bot")
                       .build())

        handlers = [
            CommandHandler('start', start_handler),
            CommandHandler('help', help_handler),
            CommandHandler('ass', direct_call),
            MessageHandler(filters.TEXT & ~filters.COMMAND, passive_call)
        ]

        for handler in handlers:
            application.add_handler(handler)

        application.add_error_handler(error_handler)

        application.run_polling(allowed_updates=Update.ALL_TYPES)
