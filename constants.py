# This file is part of Anti-Social Social Bot.
#
# Anti-Social Social Bot is free software: you can redistribute it and/or modify it under the terms of the GNU General
# Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option)
# any later version.
#
# Anti-Social Social Bot is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the
# implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more
# details.
#
# You should have received a copy of the GNU General Public License along with Anti-Social Social Bot. If not, see
# <https://www.gnu.org/licenses/>.

import json
import os


class Constants:
    """Constants used throughout the project."""
    # Env variables
    BOT_TOKEN = os.environ["BOT_TOKEN"]
    CREATOR_ID = os.environ["TELEGRAM_ID"]

    _server = os.environ["SERVER"]
    _server_port = os.environ["SERVER_PORT"]
    SERVER_URL = f"{_server}:{_server_port}"

    # Bot working directory
    DIRECTORY = "BOT_DL"
    os.makedirs(DIRECTORY, exist_ok=True)

    # Supported sites
    with open("config.json", "r") as f:
        SUPPORTED_URLS = json.loads(f.read())["url_configs"]

    # Use wireguard if a request fails
    WIREGUARD = True
    WIREGUARD_CONFIGS = 6
